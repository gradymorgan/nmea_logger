import math
from decimal import *

R = 3440.06479

def bearing(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)
    
    dLon = lon2-lon1
    
    y = math.sin(dLon)*math.cos(lat2)
    x = math.cos(lat1)*math.sin(lat2) - math.sin(lat1)*math.cos(lat2)*math.cos(dLon)
    
    return (math.degrees( math.atan2(y, x) )+360) % 360

def distance(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)

    dLat = lat2-lat1
    dLon = lon2-lon1
    
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1) * math.cos(lat2) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    
    return R * c
    



# def generateRMB(toWayp, fromWayp, lat, long, dist, brng, vmg):
#     return "GPRMB,A,,,%s,%s,%s,N,%s,W,%.2f,%d,%.2f," % (fromWayp, toWayp, lat, long, dist, brng, vmg)
# 
def navPacket(current):
## true to mag

#     try:

# data = {'lat':47.681711, 'lon':-122.405696, 'sog':4.5, 'cog':234 }
    
        fromMark = {
            'name':'SHILSHOLE',
            'lat': Decimal('47.681711'),
            'lon': Decimal('-122.405696')
        }
            
        toMark = {
            'name': 'HOME',
            'lat': Decimal('47.666534'),
            'lon': Decimal('-122.392537')
        }
        
        dist = distance( current['lat'], current['lon'], toMark['lat'], toMark['lon'] )
        brng = bearing( current['lat'], current['lon'], toMark['lat'], toMark['lon'] ) 
        
        vmg = current['sog'] * math.cos( math.radians(brng - current['cog']) ) #tk.events[i].sog * Math.cos(optcog - curcog);
        
        return {
            'to':toMark['name'],
            'from':fromMark['name'],
            'lat':toMark['lat'],
            'lon':fromMark['lon'],
            'dist-wp':dist,
            'brng-wp':brng,
            'vmg-wp':vmg
        }
#     except:
#          return None

#         cog_push = rad( currentData['hdg'] - currentData['cog'] )
#         currentData['drift'] = Math.sqrt( currentData['speed'] * currentData['speed'] + currentData['sog'] * currentData['sog'] - 2 * currentData['sog'] * currentData['speed'] * Math.cos( Math.abs(cog_push) ) )
#         currentData['set'] = deg(Math.asin( currentData['speed'] * Math.sin(cog_push) / currentData['drift'] )) + currentData['cog']

