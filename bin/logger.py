#!/usr/bin/env python

import time, math, string, sys, os, socket, optparse

import serial
from decimal import *

import NMEA, calcs

parser = optparse.OptionParser()

parser.add_option("-d", "--daemon", dest="runAsDaemon", help="whether to run as a daemon", action="store_true", default=False)
parser.add_option("-w", "--write_pid", dest="pidFile", help="destination for PID file")
#parser.add_option("-q", "--quiet", action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")

(options, args) = parser.parse_args()

if options.pidFile != None:
    options.runAsDaemon = True

def writeMessage(sentence):
    global serialPort
    
    sentence = sentence.rstrip('\r\n')
    
    out = "%s%s*%s\r\n" % ("" if sentence[0] == "$" else "$", sentence, NMEA.checksum(sentence))
#     print "::", out
    serialPort.write( out )

def logger():
    logDir = '/race/data/logs/'
    keepRunningProcess = True

    # connection globals
    serialPort = None
    broadcastSocket = None
    
    # house keeping
    lastSystemTimeSync = 0
    cleanBuffer = 0
            
    while keepRunningProcess:
        try:
            #TODOX: ls for port name?
            serialPort = serial.Serial( port = '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0042_64935343433351202160-if00', 
                                        baudrate = 115200,
                                        timeout = 5,
                                        writeTimeout = 0 )
            
            # open UDP broadcast socket
            try:
                broadcastSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                broadcastSocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            except:
                print sys.stderr, "Couldn't open udp socket"
                broadcastSocket = None

##          Write configuration messages here
            while True:
                outFile = logDir + '/' + time.strftime('%y%m%d%H.txt');
                
                s = serialPort.readline().strip()
                
                #ignore first 5 lines, they may have noise
                if cleanBuffer < 5:
                    cleanBuffer = cleanBuffer+1
                elif s:
                    with open(outFile, 'a') as f:
                        f.write( '%s\r\n' % (s) )
                        
                    if NMEA.verify(s):
                        data = NMEA.parse(s)
                        
                        #keep current picture of data
                        #expire old data
                        #run calcs 
                        #write rmb and set/drift every second
                        
                        if data != None and 't' in data and time.time() - lastSystemTimeSync > 120: #update time every 2 min
                            os.system('date +%%s -s "@%d"' % date['t'])
                            lastSystemTimeSync = time.time()
                        
                        if 'lat' in data:
                            navMsg = NMEA.RMB(calcs.navPacket(data))
                            navMsg = "$%s*%s\r\n" % (navMsg, NMEA.checksum(navMsg))
                            
                            with open(outFile, 'a') as f:
                                f.write( '%s' % (s) )
                            serialPort.write( navMsg )
                            
                        
                        if broadcastSocket != None:
                            broadcastSocket.sendto( s.rstrip('\r\n')+'\r\n', ('<broadcast>', 37001))

        except KeyboardInterrupt:
            print ""
            keepRunningProcess = False
            continue
#             sys.exit()
#         except Exception as e:
#             print "Error connecting to serial port: %s" % (e)
        #TODOX: kill signal
        finally:
            print "Finally calling"
            if broadcastSocket != None:
                broadcastSocket.close()
                broadcastSocket = None

        time.sleep(10)

if __name__ == '__main__':
    if options.runAsDaemon:
        # do the UNIX double-fork magic, see Stevens' "Advanced 
        # Programming in the UNIX Environment" for details (ISBN 0201563177)
        try: 
            pid = os.fork() 
            if pid > 0:
                # exit first parent
                sys.exit(0) 
        except OSError, e: 
            print >>sys.stderr, "fork #1 failed: %d (%s)" % (e.errno, e.strerror) 
            sys.exit(1)
    
        # decouple from parent environment
        os.chdir("/") 
        os.setsid() 
        os.umask(0) 
    
        # do second fork
        try: 
            pid = os.fork() 
            if pid > 0:
                # exit from second parent, print eventual PID before
                print "Daemon PID %d" % pid 
                sys.exit(0) 
        except OSError, e: 
            print >>sys.stderr, "fork #2 failed: %d (%s)" % (e.errno, e.strerror) 
            sys.exit(1) 
    
        # start the daemon main loop
        if options.pidFile != None:
            with open(options.pidFile, 'w') as f:
                f.write('%d'%os.getpid())
    
    #start logger
    logger()
